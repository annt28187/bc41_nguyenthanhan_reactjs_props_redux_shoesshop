import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeQuantityAction } from './redux/actions/shoesShopActions';

class CartShoes extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} style={{ width: 50 }} alt={item.name} />
          </td>
          <td>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
            >
              -
            </button>
            <strong className="mx-2">{item.number}</strong>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
            >
              +
            </button>
          </td>
          <td>{item.price}$</td>
          <td>{item.price * item.number}$</td>
          <td>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleDeleteToCart(item.id);
              }}
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Into money</th>
            <th>Action</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoesShopReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeQuantity: (idShoes, choose) => {
      dispatch(changeQuantityAction(idShoes, choose));
    },
    handleDeleteToCart: (idShoes) => {
      let action = {
        type: 'DELETE_TO_CART',
        payload: {
          idShoes: idShoes,
        },
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartShoes);
