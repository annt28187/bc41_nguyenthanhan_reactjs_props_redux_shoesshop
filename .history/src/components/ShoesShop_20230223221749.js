import React, { Component } from 'react';
import CartShoes from './CartShoes';
import DetailShoes from './DetailShoes';
import ListShoes from './ListShoes';

export default class ShoesShop extends Component {
  render() {
    return (
      <div className="container py-4">
        <h2>Shoes Shop</h2>
        <CartShoes />
        <ListShoes />
        <DetailShoes />
      </div>
    );
  }
}
