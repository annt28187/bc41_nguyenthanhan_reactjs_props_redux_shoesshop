import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addToCartAction, showDetailAction } from './redux/actions/shoesShopActions';

class ItemShoes extends Component {
  render() {
    let { image, name, price } = this.props.shoes;
    return (
      <div className="col-4 p-4">
        <div className="card border-primary h-100">
          <img className="card-img-top" src={image} alt={name} />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">{price} $</p>
            <button
              className="btn btn-primary mr-2"
              onClick={() => {
                this.props.handleAddToCart(this.props.shoes);
              }}
            >
              Add to cart
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.handleChangeDetailShoes(this.props.shoes);
              }}
            >
              Show Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoes) => {
      dispatch(addToCartAction(shoes));
    },
    handleChangeDetailShoes: (shoes) => {
      dispatch(showDetailAction(shoes));
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoes);
