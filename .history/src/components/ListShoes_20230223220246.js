import React, { Component } from 'react';
import ItemShoes from './ItemShoes';
import { connect } from 'react-redux';

class ListShoes extends Component {
  renderListShoes = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemShoes
          key={index}
          shoes={item}
          handleDetailClick={this.props.handleChangeDetailShoes}
          handleCartClick={this.props.handleAddToCart}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoes()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    listShoes: state.shoesShopReducer.listShoes,
  };
};

export default connect(mapStateToProps)(ListShoes);
