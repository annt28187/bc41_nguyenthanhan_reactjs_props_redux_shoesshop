import { dataShoes } from '../../data_shoes';

import {
  ADD_TO_CART,
  DELETE_TO_CART,
  CHANGE_QUANTITY,
  SHOW_DETAIL,
} from '../constants/shoesShopConstants';

let initialValue = {
  listShoes: dataShoes,
  detail: dataShoes[0],
  cart: [],
};

export const shoesShopReducer = (state = initialValue, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index === -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case DELETE_TO_CART: {
      let newCar = state.cart.filter((item) => {
        return item.id !== action.payload;
      });
      return { ...state, cart: newCar };
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.idShoes;
      });

      if (index !== -1) {
        cloneCart[index].number = cloneCart[index].number + action.payload.choose;
      }

      cloneCart[index].number === 0 && cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    case SHOW_DETAIL: {
      return { ...state, detail: action.payload };
    }
    default:
      return state;
  }
};
