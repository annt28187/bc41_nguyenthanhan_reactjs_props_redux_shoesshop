import { dataShoes } from '../../data_shoes';

let initialValue = {
  listShoes: dataShoes,
  detail: dataShoes[0],
  cart: [],
};

export const shoesShopReducer = (state = initialValue, action) => {
  switch (action.type) {
    case 'ADD_TO_CART': {
      let cloneCart = [...this.state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index === -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
