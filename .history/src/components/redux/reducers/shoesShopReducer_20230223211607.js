import { dataShoes } from '../../data_shoes';

let initialValue = {
  listShoes: dataShoes,
  detail: dataShoes[0],
  cart: [],
};

export const shoesShopReducer = (state = initialValue, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
