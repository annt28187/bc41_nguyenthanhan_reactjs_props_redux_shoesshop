export const ADD_TO_CART = 'ADD_TO_CART';

export const DELETE_TO_CART = 'DELETE_TO_CART';

export const CHANGE_QUANTITY = 'CHANGE_QUANTITY';

export const SHOW_DETAIL = 'SHOW_DETAIL';
