import {
  ADD_TO_CART,
  DELETE_TO_CART,
  CHANGE_QUANTITY,
  SHOW_DETAIL,
} from '../constants/shoesShopConstants';

export const addToCartAction = (shoes) => ({
  type: ADD_TO_CART,
  payload: shoes,
});

export const deleteToCartAction = (idShoes) => ({
  type: DELETE_TO_CART,
  payload: idShoes,
});

export const changeQuantityAction = (idShoes, choose) => ({
  type: CHANGE_QUANTITY,
  payload: {
    idShoes: idShoes,
    choose: choose,
  },
});
